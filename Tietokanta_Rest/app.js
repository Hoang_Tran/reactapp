const express = require("express");
//const bodyParser = require("body-parser");
/* const app = express().use(bodyParser.json()); //vanha tapa - ei enää voimassa. 
kts. https://devdocs.io/express/ -> req.body*/
var app = express();
var mysql = require('mysql')
app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true }));

/*CORS isn’t enabled on the server, this is due to security reasons by default,
so no one else but the webserver itself can make requests to the server.*/
// Add headers
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST, OPTIONS, PUT, PATCH, DELETE"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  res.setHeader("Content-type", "application/json");

  // Pass to next layer of middleware
  next();
});

//Connection configurations
var dbConnect = mysql.createConnection({
  host: "localhost",
  user: "hoang",
  password: "hoang2021",
  database:"puhelinluettelo",
  multipleStatements: true

})

//Connect to database
dbConnect.connect();

// GET all users
app.get("/users", (req, res) => {
  dbConnect.query('SELECT * FROM henkilot',function(error, results, fields) {
    if(error) throw error;
    return res.send({results, message: 'puhelinluettelo lista'});
  });  
});

// GET a user with an id
app.get('/users/:id', function (req, res) {
  let user_id = req.params.id;
  if (!user_id) {
  return res.status(400).send({ error: true, message: 'Please provide user_id' });
  }
  dbConnect.query('SELECT * FROM henkilot where id=?', user_id, function (error, results, fields) {
  if (error) throw error;
  return res.send({ error: false, data: results[0], message: 'users tiedot' });
  });
  });

// ADD an user
app.post('/user', function (req, res) {
  res.writeHead(200, {'Content-Type' : 'text/html'});
  let id = req.query.id;
  let nimi = req.query.nimi;
  let puhelin = req.query.puhelin;

  let sql = 'INSERT INTO henkilot (id, nimi, puhelin) VALUES(?, ?, ?)';

  dbConnect.query(sql, [id, nimi, puhelin], (err, results) => {
    if (err) throw err;
    res.write("Have been successfully inserted");
    res.end();
  })

});

// UPDATE an user
app.put('/users/:id', function (req, res) {
  const user_id = Number(req.params.id);
  if (!user_id) {
    return res.status(400).send({ error: true, message: 'Please provide user_id' });
    }
  let nimi = req.query.nimi;
  let puhelin = req.query.puhelin;
  let id = req.query.id;

  let sql = 'UPDATE henkilot set nimi = ?, puhelin = ? WHERE id=?';

  dbConnect.query(sql, [nimi, puhelin, id], (err, results) => {
    if (err) throw err;
    res.write("Have been successfully updated");
    res.end();
  })

});
// DELETE an user
app.delete("/users/:id", (req, res) => {
  const user_id = Number(req.params.id);
  if (!user_id) {
    return res.status(400).send({ error: true, message: 'Please provide user_id' });
    }
    
  let id = req.query.id;

  let sql = 'DELETE FROM henkilot WHERE id=?';

  dbConnect.query(sql, [id], (err, results) => {
    if (err) throw err;
    res.write("Have been successfully deleted");
    res.end();
  })
});

app.listen(3000, () => {
  console.log("Server listening at port 3000");
});
